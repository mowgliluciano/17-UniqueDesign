const mongoose = require('mongoose');

const clothingItemsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true,
    },
    frontImage: {
        type: String,
        required: true,
    },
    backImage: {
        type: String,
        required: true,
    },
});
 
const clothingItemsModel = mongoose.model('clothingitems', clothingItemsSchema);
module.exports = clothingItemsModel;