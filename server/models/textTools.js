const mongoose = require('mongoose');
const uuid = require('uuid');


const textToolsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    textContent: {
        type: String,
        required: true,
    },
    size: {
        type: Number,
        required: true,
    },
    color: {
        type: String,
        required: true,
    },
    positionX: {
        type: Number,
        required: true,
    },
    positionY: {
        type: Number,
        required: true,
    },
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'products',
        required: true,
    },
    flag: {
        type: String,
        enum: ['f', 'b'],
        required: true,
    },
});


const textToolsModel = mongoose.model('texttools', textToolsSchema);
module.exports = textToolsModel;
