const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const productsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true,
    },
    size: {
        type: String,
        required: true,
    },
    color: {
        type: String,
        required: true,
    },
    public: {
        type: Boolean,
        default: false,
    },
    price: {
        type: Number,
        default: 100,
    },
    idClothes: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'clothingItems',
        required: true,
    },
    idUser: {
        type: String,
        required: true,
    }
});

productsSchema.plugin(mongoosePaginate);

const productsModel = mongoose.model('products', productsSchema);

async function paginateThroughProducts(page = 1, limit = 10) {
    return await productsModel.paginate({}, { page, limit});
}
  
async function paginateThroughPublicProducts(page = 1, limit = 10) {
    return await productsModel.paginate({public: true}, { page, limit});
}

async function paginateThroughProductsByUsername(username, page = 1, limit = 10) {
    return await productsModel.paginate({idUser: username}, { page, limit});
}
module.exports =  {
    productsModel,
    paginateThroughProducts,
    paginateThroughPublicProducts,
    paginateThroughProductsByUsername
}