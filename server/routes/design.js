const express = require('express');
const router = express.Router();

const designController = require('../controllers/design');

router.get("/",designController.index);

module.exports = router;