const express = require('express');
const userController = require('../controllers/user');
const authJWT = require('../utils/authentication');

const router = express.Router();

router.post("/register", userController.register);

router.post("/login", userController.login);

router.put("/", authJWT.verifyToken, userController.changeUserPassword);

router.put("/update", authJWT.verifyToken, userController.updateUser);

router.put("/updateProfileImage", authJWT.verifyToken, userController.changeUserProfileImage);

// router.delete("/", authJWT.verifyToken ,userController.deleteUserById);
// router.get("/", authJWT.verifyToken, userController.getUserByUsername);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json(`mesage: Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!`);
});

module.exports = router;