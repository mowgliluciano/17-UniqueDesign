const mongoose = require('mongoose');
const {productsModel, paginateThroughProducts, paginateThroughPublicProducts} = require('../models/products');

const getAllProducts  = async (req, res, next) => {
    const page = Number.parseInt(req.query.page);
    const limit = Number.parseInt(req.query.limit);
    try{
        const products = await paginateThroughProducts(page, limit);
        res.status(201).json(products.docs);
    } catch(err){
        next(err);
    }
};

const getAllPublicProducts  = async (req, res, next) => {
    const page = Number.parseInt(req.query.page);
    const limit = Number.parseInt(req.query.limit);

    try{
        const products = await paginateThroughPublicProducts(page, limit);
        res.status(200).json(products.docs);
    } catch(err){
        next(err);
    }
};

const getProductById = async (req, res, next) => {
    try{
        const product = await productsModel.findById(req.params.id).exec();
        if(!product){
            res.status(404).json("Ne postoji produkt sa zadatim id-em");
        } else{
            res.status(200).json(product);
        }
    } catch(err){
        next(err);
    }
};

const addNewProduct = async (req, res, next) => {
    const {name, size, public, idClothes, idUser, color} = req.body;
    
    if((!name) || (!size) || !(public !== undefined) || (!idClothes) || (!idUser)){
        res.status(400).json("Greska u zahtevu, neki parametar fali!");
    } else{
        try{
            const newProduct = new productsModel({
                _id: new mongoose.Types.ObjectId(),
                name: name,
                size: size,
                public: public,
                idClothes: idClothes,
                idUser: idUser,
                color: color
            });
            await newProduct.save();
            res.status(200).json(newProduct);
        } catch(err){
            next(err);
        }
    }
};

const changeProduct = async(req, res, next) => {
    const {name, size, public, idClothes} = req.body;

    if((!name) || (!size) || !(public !== undefined) || (!idClothes)){
        res.status(400).json("Greska u zahtevu, neki parametar fali!");
    } else{
        try{
            const product = await productsModel.findById(req.params.id).exec();
            if (!product){
                res.status(404).json("Ne postoji produkt sa zadatim id-em");
            } else{
                product.name = name;
                product.size = size;
                product.public = public;
                product.idClothes = idClothes;
                await product.save();
                res.status(200).json("Uspesno ste azurirali produkt");
            }
        } catch(err){
            next(err);
        }
    }
};

const deleteProduct = async (req, res, next) => {
    try{
        const product = await productsModel.findById(req.params.id).exec();
        if(product){
            await productsModel.findByIdAndDelete(req.params.id).exec();
            res.status(200).json("Uspesno ste obrisali produkt!");
        } else{
            res.status(404).json("Neuspelo brisanje produkta!");
        }
    } catch(err){
        next(err);
    }
};

const getProductsByUsername = async (req, res, next) => {
    const usernameInToken = req.username;    
    // const page = Number.parseInt(req.query.page);
    // const limit = Number.parseInt(req.query.limit);
    try{
        const products = await productsModel.find({idUser: usernameInToken}).exec();
        res.status(200).json(products);        
    }catch(err){
        next(err);
    }
}

module.exports = {
    getAllPublicProducts,
    getProductById,
    getAllProducts,
    addNewProduct,
    changeProduct,
    deleteProduct,
    getProductsByUsername
};