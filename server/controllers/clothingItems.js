const mongoose = require('mongoose');
const clothingItemsModel = require('../models/clothingItems');

const getAllClothingItems  = async (req, res, next) => {

    try{
        const clothingItems = await clothingItemsModel.find({}).exec();
        res.status(200).json(clothingItems);
    } catch(err){
        next(err);
    }
};

const getClothingItemById = async (req, res, next) => {

    try{
        const clothingItem = await clothingItemsModel.findById(req.params.id).exec();
        if(!clothingItem){
            res.status(404).json("Ne postoji odevni predmet sa zadatim id-em");
        } else{
            res.status(200).json(clothingItem);
        }
    } catch(err){
        next(err);
    }
};

const addNewClothingItem = async (req, res, next) => {
    const {name, frontImage, backImage} = req.body;
    
    if((!name) || (!frontImage) || (!backImage)){
        res.status(400).json("Greska u zahtevu, neki parametar fali!");
    } else{
        try{
            const newClothingItem = new clothingItemsModel({
                _id: new mongoose.Types.ObjectId(),
                name: name,
                frontImage: frontImage,
                backImage: backImage
            });
            await newClothingItem.save();
            res.status(200).json(newClothingItem);
        } catch(err){
            next(err);
        }
    }
};

const deleteClothingItem = async (req, res, next) => {
    try{
        const clothingItem = await clothingItemsModel.findById(req.params.id).exec();
        if(clothingItem){
            await clothingItemsModel.findByIdAndDelete(req.params.id).exec();
            res.status(200).json("Uspesno ste obrisali odevni predmet!");
        } else{
            res.status(404).json("Neuspelo brisanje odevnog predmeta!");
        }
    } catch(err){
        next(err);
    }
};

module.exports = {
    getAllClothingItems,
    getClothingItemById,
    addNewClothingItem,
    deleteClothingItem
};