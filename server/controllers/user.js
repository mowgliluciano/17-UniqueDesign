const userModel = require('../models/users');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { jwt_secret, SALT_ROUNDS } = require('../config/auth.config');
const {uploadFile} = require('./upload');

const getAllUsers = async (req, res, next) => {
    try {
        const users = await userModel.find({}).exec();
        res.status(200).json(users);
    } catch (err) {
        next(err);
    }
};

const getUserByUsername = async (req, res, next) => {
    try {
        const username = req.username;
        const user = await userModel.findOne({ username: username }).exec();
        if (!user) {
            throw getError("Prosledjeni username ne postoji!", 404)
        }
        res.status(200).json(user);
    } catch (err) {
        next(err);
    }
};

const updateUser = async (req, res, next) =>{
   try{
    const {name, surname, email, username} = req.body;
    const usernameInToken = req.username;
    if(username !== usernameInToken){
        throw getError("Unauthorized!", 403);
    }

    const user = await userModel.findOne({username: username}).exec();
    user.name = name;
    user.surname = surname;
    user.email = email;
    await user.save();
    const userToken = jwt.sign({ name: user.name, surname: surname, username: user.username, password: user.password, email: user.email,  imgUrl:user.imgUrl}, jwt_secret, { expiresIn: '7d' });
    res.status(200).json({
        token: userToken
    });
   }catch(err){
       throw err;
   }
}


const register = async (req, res, next) => {
    try {
        const { name, surname, username, password, email } = req.body;
        if ((!name) || (!surname) || (!username) || (!password) || !(email)) {
            throw getError("Greska u zahtevu, neki paramtear fali!", 400);
        }

        const user = await userModel.findOne({ username: username }).exec();
        if (user) {
            throw getError(`${username} vec postoji!`, 303);
        }

        const hashedPassword = await bcrypt.hash(password, SALT_ROUNDS);
        const newUser = new userModel({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            surname: surname,
            username: username,
            password: hashedPassword,
            email: email,
            status: 'active',
            admin: false,
        });
        await newUser.save();
       const userToken = jwt.sign({ name: name, surname: surname, username: username, password: password, email: email}, jwt_secret, { expiresIn: '7d' });
       res.status(200).json({
        token: userToken
    });
    } catch (err) {
        next(err);
    }
};

const changeUserPassword = async (req, res, next) => {
    try {
        const { username, oldPassword, newPassword } = req.body;
        const usernameInToken = req.username;
        if ((!oldPassword) || (!newPassword) || (!username)) {
            throw getError("Fali neki parametar", 403);
        }
        if (username !== usernameInToken) {
            throw getError("Nemate pravo da izvrsite ovu operaciju!", 403);
        }

        const user = await userModel.findOne({ username: username }).exec();
        if (!user) {
            throw getError(`Ne postoji ${username}`, 404);
        }

        const isMatched = await bcrypt.compare(oldPassword, user.password);
        if (!isMatched) {
            throw getError("Stara lozinka i prosledjena lozinka se nepoklapaju", 402);
        }

        user.password = await bcrypt.hash(newPassword, SALT_ROUNDS);
        await user.save();
        const userToken = jwt.sign({ name: user.name, surname: user.surname, username: user.username, password: user.password, email: user.email,  imgUrl:user.imgUrl}, jwt_secret, { expiresIn: '7d' });
        res.status(200).json({token: userToken});
    } catch (err) {
        next(err);
    }
};

const login = async (req, res, next) => {
    try {
        const { username, password } = req.body;
        if (!username || !password) {
            throw getError("Nije prosledjen username ili password!", 400);
        }

        const user = await userModel.findOne({ username: username }).exec();
        if (!user) {
            throw getError(`Username ${username} ne postoji!`, 401);
        }

        const isMatched = await bcrypt.compare(password, user.password);
        if (!isMatched) {
            throw getError("Uneli ste pogresnu lozinku!", 401);
        }

        const userToken = jwt.sign({ id: user._id, name: user.name, surname: user.surname, username: user.username, password: user.password, email: user.email, imgUrl:user.imgUrl }, jwt_secret, { expiresIn: '7d' });
        res.status(200).json({token: userToken});
    } catch (err) {
        next(err);
    }
};

const changeUserProfileImage = async (req, res, next) => {
    try{
        const usernameInToken = req.username;
        await uploadFile(req, res);

        if (req.file == undefined) {
            throw getError("Fajl nije prosledjen!", 400);
          }
          
        const imgUrl = req.file.filename;
        const user = await userModel.findOne({username:usernameInToken}).exec();
        user.imgUrl = imgUrl;
        await user.save();
        const userToken = jwt.sign({ name: user.name, surname: user.surname, username: user.username, password: user.password, email: user.email, imgUrl:user.imgUrl }, jwt_secret, { expiresIn: '7d' });
        res.status(200).json({token: userToken});
    }catch(err){
        throw err;
    }
}

const getError = (message, statusCode) => {
    const error = new Error(message);
    error.status = statusCode;
    return error;
}

module.exports = {
    getAllUsers,
    getUserByUsername,
    register,
    changeUserPassword,
    login,
    updateUser,
    changeUserProfileImage
};