const mongoose = require('mongoose');
const textToolsModel = require('../models/textTools');

const getAllTextTools = async (req, res, next) =>{
    try{
        const textTools = await textToolsModel.find({productId: req.query.productId}).exec();
        res.status(200).json(textTools);
     } catch(err){
        next(err);
    }
};


const getTextToolById = async (req, res, next) => {
    try{
        const textTool = await textToolsModel.findById(req.params.id).exec();
         if(!textTool){
            res.status(404).json("Ne postoji textTool sa zadatim id-em");
        }
        else{
            res.status(200).json(textTool);
        }
    }
    catch(err){
        next(err);
    }
}


const addNewTextTool = async (req, res, next) => {
    const {textContent, size, color, positionX, positionY, productId, flag} = req.body;
    
    if ((!textContent) || (!size) || (!color) || (!positionX) || (!positionY) || (!productId) ){
        res.status(400).json("Greska u zahtevu, neki parametar fali!");
    } else {
        try{
            const newTextTool = new textToolsModel({
                _id: new mongoose.Types.ObjectId(),
                textContent: textContent,
                size: size,
                color: color,
                positionX: positionX,
                positionY: positionY,
                productId: productId,
                flag: flag
            });
            await newTextTool.save();
            res.status(201).json(newTextTool);
        }
        catch(err){
            next(err);
        }
    }
};

const changeTextTool = async(req, res, next) => {
    const {newTextContent, newSize, newColor, newPositionX, newPositionY, newProductId, newFlag} = req.body;

    if ((!newTextContent) || (!newSize) || (!newColor) || (!newPositionX) || (!newPositionY) || (!newProductId) || (!newFlag)){
        res.status(400).json("Greska u zahtevu, neki parametar fali!");
    }
    else {
        try{
            const textTool = await textToolsModel.findById(req.params.id).exec();
            if (!textTool){
                res.status(404).json("Ne postoji textTool sa zadatim id-em");
            }
            else {
                textTool.textContent = newTextContent;
                textTool.size = newSize;
                textTool.color = newColor;
                textTool.positionX = newPositionX;
                textTool.positionY = newPositionY;
                textTool.productId = newProductId;
                textTool.flag = newFlag;

                await textTool.save();
                res.status(200).json("Uspesno ste azurirali tekst");
            }
        }catch(err){
            next(err);
        }
    }
};


const deleteTextTool = async (req, res, next) => {
    try{
        const textTool= await textToolsModel.findById(req.params.id).exec();
        if(textTool){
            await textToolsModel.findByIdAndDelete(req.params.id).exec();
            res.status(200).json("Uspesno ste obrisali tekst!");
        }
        else{
            res.status(404).json("Neuspelo brisanje teksta!");
        }
    }catch(err){
        next(err);
    }
};

module.exports = {
    getAllTextTools,
    getTextToolById,
    addNewTextTool,
    changeTextTool,
    deleteTextTool
};