import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { StoreProduct } from '../models/store-product';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

import { StoreService } from '../services/store.service';
@Component({
  selector: 'app-store-product',
  templateUrl: './store-product.component.html',
  styleUrls: ['./store-product.component.css']
})
export class StoreProductComponent implements OnInit, OnDestroy {

  storeProduct: StoreProduct;

  activeSubscriptions: Subscription[];

  constructor(private route: ActivatedRoute, private storeService: StoreService) {
    this.activeSubscriptions = [];
    const getSub = this.route.paramMap
      .pipe(
        map((paramMap) => paramMap.get('_id')),
        switchMap((_id) => this.storeService.getProductById(_id))
      )
      .subscribe((storeProduct) => {
        this.storeProduct = storeProduct;
      });
  }

  ngOnInit(): void {
  }
  
  ngOnDestroy() {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

}
