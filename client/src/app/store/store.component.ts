import { OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';


import { StoreService } from '../store/services/store.service';
import { Product } from '../old-products/models/product';
import { StoreProduct } from './models/store-product';
import { nameValidator } from './name.validator';


@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  public products: Product[];
  public checkout: FormGroup;
  private activeSubscriptions: Subscription[];


  constructor(
    private formBuilder: FormBuilder,
    private storeService: StoreService) { 
    this.activeSubscriptions = [];
    this.products = this.storeService.getItems();


    this.checkout = this.formBuilder.group({
      name: ['', [Validators.required, nameValidator()]],
      address : ['', [Validators.required, Validators.pattern('[0-9]+ [ a-zA-Z0-9]+')]],
      email: ['', [Validators.required, Validators.email]],
    });
    
  }
  ngOnInit(): void {

  }

  ngOnDestroy() {
    this.activeSubscriptions.forEach((sub) => {
      sub.unsubscribe();
    });

  }

  public submitForm(data): void {
    if (!this.checkout.valid) {
      window.alert('Not valid!');
      return;
    }

    const createSub = this.storeService
      .createAnOrder(data)
      .subscribe((product: StoreProduct) => {
        window.alert(
          `Your order (number: ${product._id}) is successfully created!`
        );
        this.products = this.storeService.clearCart();
        this.checkout.reset();
      });
    this.activeSubscriptions.push(createSub);
  }

  public get name() {
    return this.checkout.get('name');
  }
  public get address() {
    return this.checkout.get('address');
  }
  public get email() {
    return this.checkout.get('email');
  }
}
