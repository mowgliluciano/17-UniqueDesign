import { Subscription, Observable } from 'rxjs';
import { AuthService } from './../services/auth.service';
import { User } from './../models/user.model';
import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy{

  user:User = null;
  sub: Subscription;
  userForm: FormGroup;
  userPasswordForm: FormGroup;
  userImageForm: FormGroup;
  imageForUpload: File = null;

  constructor(public auth: AuthService, private userService: UserService, private router: Router){
    auth.user.subscribe((user:User)=>{
       this.user=user;
      });
    this.user = this.auth.sendUserDataIfExists();
  }

  public onClickLogout(): void{
    this.auth.logoutUser();
    this.router.navigate(["/login"]);
  }

  ngOnInit(): void {
      this.userForm = new FormGroup({
        name: new FormControl(this.user.name, [Validators.required, Validators.pattern(new RegExp("^[A-Z][a-zA-Z]{1,19}$"))]),
        surname: new FormControl(this.user.surname, [Validators.required, Validators.pattern(new RegExp("^[A-Z][a-zA-Z]{1,19}$"))]),
        email: new FormControl(this.user.email, [Validators.required, Validators.email])
      });

      this.userPasswordForm = new FormGroup({oldPassword: new FormControl("", [Validators.required, Validators.minLength(6)]),
        newPassword: new FormControl("", [Validators.required, Validators.minLength(6)])});

      this.userImageForm = new FormGroup(
        { image: new FormControl("",[Validators.required])});
  }

  ngOnDestroy():void {
    if(this.sub){ this.sub.unsubscribe(); }
  }

  public onUserFormSubmit(){
    if(this.userForm.invalid){
      window.alert("Formular is not valid!");
      return;
    }

    const data = this.userForm.value;

    this.userService.updateUser(this.user.username,data.name,data.surname,data.email)
      .subscribe((user: User)=>{
    this.user.name = user.name;
    this.user.surname = user.surname;
    this.user.email = user.email;

    this.userForm.reset({
      name: this.user.name,
      surname: this.user.surname,
      email: this.user.email});
    });
    window.alert("Successfully updated the data");
  }

  public onUserPasswordSubmit(){
    if(this.userPasswordForm.invalid){
      window.alert("Formular is not valid");
      return;
    }
    const {oldPassword, newPassword} = this.userPasswordForm.value;

    this.userService.updateUserPassword(this.user.username, oldPassword, newPassword)
    .subscribe((user: User) => {
      this.user.password = user.password;
      console.log(this.user);
      window.alert("Successfully updated the password");
      },err => {
        window.alert("Old and forwarded passwords do not match");
      });

      this.userPasswordForm.reset({
        oldPassword:"",
        newPassword:""
      });
  }

  public hasErrors(formName: string) : boolean{
      const errors = this.userForm.get(formName).errors;
      return errors !== null;
  }

  public hasPassErrors(formName: string) :boolean{
    const errors = this.userPasswordForm.get(formName).errors;
    return errors !== null;
  }

  public errorMessages(formName: string): string[]{
    const errors = this.userForm.get(formName).errors;
    if(errors === null){
      return [];
    }
    const messages: string[] = [];

    if( errors.pattern){
      messages.push("Must start with a capital letter and contain 2 to 20 letters of the alphabet");
    }
    if(errors.required){
      messages.push("Field is required");
    }
    if(errors.email){
      messages.push("Invalid email");
    }
    return messages;
  }

  public passErrorMessages(formName: string):string[]{
    const errors = this.userPasswordForm.get(formName).errors;
    if(errors === null){
      return [];
    }

    const messages: string[] = [];
    if(errors.required){
      messages.push("Field is required");
    }
    if(errors.minlength){
      messages.push("Password must contain a minimum of 6 characters");
    }
    return messages;
  }

  public getImageUrl(): string{
    return this.user.imgUrl;
  }

  public onChangeFile(event: Event){
    const files: FileList = (event.target as HTMLInputElement).files;
    if(files.length === 0){
      this.imageForUpload = null;
      return;
    }
    this.imageForUpload = files[0];

  }
  public onUserImageSubmit(){
    if(this.userImageForm.invalid){
      window.alert("File not selected");
      return;
    }

    this.userService.updateProfileImage(this.imageForUpload).subscribe((user: User)=>{
      this.user = user;
      this.userImageForm.reset({
        image:""});
      });
  }
}
