import { Component, OnInit, ElementRef, ViewChild, EventEmitter, Output, OnDestroy } from '@angular/core';
import { User } from '../models/user.model';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit, OnDestroy {

  registerForm: FormGroup;
  userSub: Subscription;
  showForm = true;


  constructor(private authService: AuthService, private router: Router) {
    this.registerForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern('[A-Z][a-z]+')]),
      surname: new FormControl('', [Validators.required, Validators.pattern('[A-Z][a-z]+')]),
      email: new FormControl('', [Validators.required, Validators.email]),
      username: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9A-Z]{6,12}')]),
      password: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9A-Z]{6,12}')]),
      password1: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9A-Z]{6,12}')])
    });
  }

  ngOnInit(): void {

  }
  ngOnDestroy(): void {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }

  }

  public onRegisterFormSubmit(): void {
    const data = this.registerForm.value;
    if (this.registerForm.invalid) {
      window.alert('Form is not valid!');
      setTimeout(() => {
        this.showForm = true;
        this.registerForm.reset();
      }, 1000);
      return;
    }
    if (data.password !== data.password1) {
      window.alert('Passwords do not match!');
      setTimeout(() => {
        this.showForm = true;
        this.registerForm.reset();
      }, 1000);
      return;
    }

    const obsUser: Observable<User> = this.authService.registerUser(
      this.registerForm.value.name,
      this.registerForm.value.surname,
      this.registerForm.value.username,
      this.registerForm.value.password,
      this.registerForm.value.email
    );
    const sub: Subscription = obsUser.subscribe((user: User) => {
      window.alert('Account successfully created');
      this.showForm = false;

    }, (err) => {
      if (err.error) {
        window.alert(err.error.message);
        this.showForm = true;
      }
    });
    console.log(sub);
    this.userSub = sub;
    this.registerForm.reset();

  }

  public hasError(inputErr: string): boolean {
    const errors: ValidationErrors = this.registerForm.get(inputErr).errors;
    return errors != null;

  }

  public errorsFromInput(input: string): string[] {
    const errors: ValidationErrors = this.registerForm.get(input).errors;
    if (errors === null) {
      return [];
    }
    const errorMessages: string[] = [];
    if (errors.required) {
      errorMessages.push('This field is required!');
    }
    if (errors.pattern) {
      if (input === 'name' || input === 'surname') {
        errorMessages.push('First initial letter in ' + input + ' must be uppercase');
      }
      else if (input === 'username' || input === 'password' || input === 'password1') {
        errorMessages.push('Alphabet characters or digits in range 6-12');
      }
    }
    if (errors.email) {
      errorMessages.push('Email must have @ character');
    }
    return errorMessages;

  }

}
