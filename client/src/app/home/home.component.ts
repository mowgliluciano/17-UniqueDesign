import { Component, OnInit } from '@angular/core';
import { User } from '../users/models/user.model';
import { Login } from '../users/models/login.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public hide = false;

  constructor() { }

  ngOnInit(): void {
  }

  public onShowComponent(show: boolean): void {
    this.hide = show;
  }

}
