import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './users/login/login.component';
import { RegistrationComponent } from './users/registration/registration.component';
import { UserInfoComponent } from './users/user-info/user-info.component';
import { UserProductsComponent } from './users/user-info/user-products/user-products.component';
import { DesignComponent } from './designProduct/design/design.component';
import { TextComponent } from './designProduct/text/text.component';
import { ImageComponent } from './designProduct/image/image.component';
import { CanvasComponent } from './designProduct/canvas/canvas.component';
import { ProductListComponent } from './old-products/product-list/product-list.component';
import { ProductComponent } from './old-products/product/product.component';
import { StoreComponent } from './store/store.component';
import { StoreProductsComponent } from './store/store-products/store-products.component';
import { StoreProductComponent } from './store/store-product/store-product.component';
import { MatIconModule } from '@angular/material/icon';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NavMenuComponent } from './common/nav-menu/nav-menu.component';
import { SumPipe } from './old-products/pipes/sum.pipe'; 

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegistrationComponent,
    UserInfoComponent,
    UserProductsComponent,
    DesignComponent,
    TextComponent,
    ImageComponent,
    CanvasComponent,
    ProductListComponent,
    ProductComponent,
    StoreComponent,
    StoreProductsComponent,
    StoreProductComponent,
    NavMenuComponent,
    SumPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatIconModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
