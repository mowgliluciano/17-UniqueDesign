import { UserAuthenticatedGuard } from './users/guards/user-authenticated.guard';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserInfoComponent } from './users/user-info/user-info.component';
import { DesignComponent } from './designProduct/design/design.component';
import { ProductListComponent } from './old-products/product-list/product-list.component';
import { StoreComponent } from './store/store.component';
import { UserLoginGuard } from './users/guards/user-login.guard';
import { ProductComponent } from './old-products/product/product.component';


const routes: Routes = [
  {path: 'login',component:HomeComponent, canActivate:[UserLoginGuard]},
  {path: 'user',component:UserInfoComponent, canActivate:[UserAuthenticatedGuard]},
  {path: 'products',component:UserInfoComponent},
  {path: 'design/:productId',component:DesignComponent},
  {path: 'design',component:DesignComponent},
  {path: 'product-list',component:ProductListComponent},
  {path: 'store',component:StoreComponent},
  {path: 'products/:productId', component:ProductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
