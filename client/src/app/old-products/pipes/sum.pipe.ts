import { Pipe, PipeTransform } from '@angular/core';
import { Product } from './../models/product';

@Pipe({
  name: 'sum'
})
export class SumPipe implements PipeTransform {

  transform(products: Product[]): number {
    return products
      .map(product => product.price)
      .reduceRight((acc, next) => acc + next);
  }
  

}
