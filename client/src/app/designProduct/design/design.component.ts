import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Image } from '../models/image.model';
import { Product } from '../models/product.model';
import { Text } from '../models/text.model';
import { DesignService } from '../services/design.service';

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.css']
})
export class DesignComponent {

  selectedTool;
  product: Product = new Product();

  constructor(
    private sDesign: DesignService,
    private activatedRoute: ActivatedRoute
  ) {
    /**
     * dohvatanje parametara url-a
     */
    this.activatedRoute.params.subscribe(params => {
      const productId: string = params.productId;
      /**
       * prosledjen id produkta -> dohvatanje produkta sa backend-a
       */
      if (productId) {
        this.sDesign.getProductById(productId).subscribe(res => {
          this.product = new Product(
            res._id,
            res.name,
            res.size,
            res.color,
            res.public,
            res.idClothes,
            res.idUser
          );
        });
      }
    })
  }
  
  /**
   * indeikator da li je selektovan img alat
   */
  get isImgToolSelected () {
    return this.selectedTool instanceof Image;
  }

  /**
   * indikator da li je selektovan text alat
   */
  get isTextToolSelected () {
    return this.selectedTool instanceof Text;
  }
}
