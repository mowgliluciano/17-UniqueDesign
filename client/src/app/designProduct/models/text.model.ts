export class Text{
    public constructor(
        public textContent: string,
        public size: number,
        public color: string,
        public positionX: number, 
        public positionY: number,
        public flag: string
    ) {}
}