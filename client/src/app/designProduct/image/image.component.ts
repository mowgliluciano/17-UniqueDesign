import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent {
  @Input() tool;

  constructor() { }

  /**
   * @param fileInput -> informacije o change event-u 
   * 
   * funkcija koja ucitanu sliku prebacuje u base64 format
   * i tu vrednost cuva u okviru izabranog image alata
   */
  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
          // TODO probati bez image 
          const image = new Image();
          image.src = e.target.result;
          image.onload = rs => {
            const imgBase64Path = e.target.result;
            this.tool.imageContent = imgBase64Path;
          };
      };

      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

}
